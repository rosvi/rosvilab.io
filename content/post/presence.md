+++
categories = ["Development", "VIM"]
date = "2018-10-12"
slug = "presence"
title = "Presence"
+++

A presença social se faz importante. Não poder partilhar e expressar me faz sentir que pouco a pouco me faço menos presente em mim mesma. Menos capaz de perceber o interno e o externo pela falta de palavras, de saber-pensar.

Conectar-me aos outros é conectar-me a mim mesma, quem diria. Yogico, do sânscrito para união.

É uma forma limitada, caricata, mas escrever ao mundo é como posso me perceber no mundo, um mundo povoado. Frequentado, talvez também por mim. A imagem de uma vida passando pelo mundo, nunca o tocando. Atravessava o mundo sem deixar pegadas; um prazer doentio em não-ser. Ver sem ser vista.

A sugestão voyeurística nisso contrasta com a experiência de observação distante, clínica, desinteressada. Cada vez mais desinteressada. A minúcia é entediante e os detalhes voejam. O mundo por vezes era algum conceito abstrato, alguma impossibilidade semântica.

Uma vez, voluntariamente a contragosto como o foram todas minhas experiências com a Aya, tive uma miração. Coisa besta, inexplicável, onírica: vi em mim algo negro, rabiscado violentamente, seu grafite rasgando a superfície daquilo que o podia perceber em primeiro lugar: o que quer que se podia entender como "mim" que lhe servia de papel preenchendo aquele buraco-negro tomando conta de mim.

Por vezes o mundo é uma impossibilidade semântica e você é apenas aquele buraco-negro. Ver sem ser vista é catatonia.

Por isso escrevo, aperto retângulos dispostos de maneira especialmente disfuncional cinesiologicamente que modulam fênomenos eletromagnéticos de acordo com padrões arbitrários pré-definidos que se transformam, através de uma incontável séries de transformações arbitrárias mas pré-combinadas, em fenômenos eletromagnéticos que, por acaso da seleção natural, são percebidos por mim como linhas pretas em um fundo preto. Essas linhas importam, creio, pois não sou apenas eu quem as consigo ler.

Desses retângulos posso também dizer que as linhas brancas que rotulam essas próprias teclas com o que chamamos de letras não correspondem ao resultado final exibido no monitor. Por uma série de motivos aprendi anos atrás a datilografar em teclados dvorak. 


Esse é o tipo de detalhe que me foge se é relevante ou não. O digo aqui por medo de que se não o disser cegarei das palavras. Informação absolutamente extrânea, é o que julgo. Mas é uma parte de mim que agora se encontra registrada. Um caminho atravessado no qual deixei pegadas nas quais talvez não se possa me ver ― não sei mesmo se creio sequer que posso ser verdadeiramente vista por mim ou outrem ― mas se possa talvez me supor, com sorte deduzir-se minha passagem.

São pegadas. Ando tentando deixá-las. 







