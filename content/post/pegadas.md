---
title: "Pegadas"
date: 2018-10-12T23:54:12-03:00
draft: false
---


Uma marca na trilha


É bastante importante para mim que o travessão no texto anterior seja inserido, transmitido e exibido em sua representação binária arbitrária exata no que chamam de UTF-8. Dois hífens não servem, sinal de menos também não. Apenas a representação binária exata de um travessão enquanto esse maravilhoso conceito de "caractere" único, distinguível de quaisquer outros caracteres mesmo que sejam visualmente idênticos. Existe um interponto, um sinal de multiplicação e um marcador de lista. Qualquer pessoa a punho os faria idênticos, a maioria das fontes os representa identicamente. Mas os padrões elétricos que definimos para os representar são distintos e concebidos deliberadamente assim.

Tentar ver e tornar visíveis essas pegadas às vezes me faz perceber massas de pegadas e conceber caminhos cruzados em escalas macroscópicas. 
